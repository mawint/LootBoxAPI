'use strict';

const rp = require('request-promise');
const cheerio = require('cheerio');
const Joi = require('joi');

const getProfile = function (request,h) { // eslint-disable-line
  const tag = encodeURI(request.params.tag);
  const region = encodeURIComponent(request.params.region);
  const platform = encodeURIComponent(request.params.platform);

  return new Promise(function(resolve, reject) {
    let url = `https://playoverwatch.com/en-us/career/${platform}/${region}/${tag}`;

    if (platform === 'psn' || platform === 'xbl') {
      url = `https://playoverwatch.com/en-us/career/${platform}/${tag}`;
    }

    rp(url)
      .then(function(htmlString){
        const $ = cheerio.load(htmlString);
        const Username = $('.header-masthead').text();

        const gamesWon = {};
        const gamesPlayed = {};
        const timeplayed = {};
        const lost = {};
        let competitiveRank;
        let competitiveRankImg;
        let Star = '';

        const quickgamesWonElm = $('#quickplay td:contains("Games Won")').next().html();
        const quickgamesPlayedElm = $('#quickplay td:contains("Games Played")').next().html();
        const quicktimePlayedElm = $('#quickplay td:contains("Time Played")').next().html();

        const compgamesWonElm = $('#competitive td:contains("Games Won")').next().html();
        const compgamesPlayedElm = $('#competitive td:contains("Games Played")').next().html();
        const comptimePlayedElm = $('#competitive td:contains("Time Played")').next().html();
        const competitiveRankElm = $('.competitive-rank');
        const compgameTie = $('#competitive td:contains("Games Tied")').next().html();

        const LevelFrame = $('.player-level').attr('style').slice(21, 109);
        const starElm = $('.player-level').html();

        const playerLevel = $('.masthead-player .player-level > div').text();
        const playerAvatar = $('.masthead-player img').attr('src');

        if (competitiveRankElm != null) {
          competitiveRankImg = $('.competitive-rank img').attr('src');
          competitiveRank = $('.competitive-rank div').html();
        }

        if (quickgamesWonElm != null) {
          gamesWon.quick = quickgamesWonElm.trim().replace(/,/g, '');
        }

        if (quickgamesPlayedElm != null) {
          gamesPlayed.quick = quickgamesPlayedElm.trim().replace(/,/g, '');
          lost.quick = gamesPlayed.quick - gamesWon.quick;
        }

        if (quicktimePlayedElm != null) {
          timeplayed.quick = quicktimePlayedElm.trim().replace(/,/g, '');
        }

        if (compgameTie != null) {
          gamesWon.compTie = compgameTie.trim().replace(/,/g, '');
        }

        if (compgamesWonElm != null) {
          gamesWon.comp = compgamesWonElm.trim().replace(/,/g, '');
        }

        if (compgamesPlayedElm != null) {
          gamesPlayed.comp = compgamesPlayedElm.trim().replace(/,/g, '');
          if(gamesWon.compTie){
            gamesPlayed.comp = (gamesPlayed.comp)-(gamesWon.compTie);
          }
          lost.comp = gamesPlayed.comp - gamesWon.comp;
        }

        if (comptimePlayedElm != null) {
          timeplayed.comp = comptimePlayedElm.trim().replace(/,/g, '');
        }

        if (starElm != null) {
          Star = $('.player-level').attr('style').slice(21, 107);
        }

        const result = {
          data: {
            username: Username,
            level: playerLevel,
            games: {
              quick: { wins: gamesWon.quick, lost: lost.quick, played: gamesPlayed.quick },
              competitive: { wins: gamesWon.comp, lost: lost.comp, played: gamesPlayed.comp },
            },
            playtime: { quick: timeplayed.quick, competitive: timeplayed.comp },
            avatar: playerAvatar,
            competitive: { rank: competitiveRank, rank_img: competitiveRankImg },
            levelFrame: LevelFrame,
            star: Star,
          }
        };

        resolve(result);

    }).catch(function(e) {
      const result = {
        statusCode: 404,
        error: `Found no user with the BattleTag: ${tag}`
      };

      resolve(result);
    });

  });
};



exports.plugin = {
  register: (server, options) => { // eslint-disable-line
    server.method('getProfile', getProfile, {
      cache: {
        cache: 'mongo',
        expiresIn: 6 * 10000, // 10 minutes
        generateTimeout: 40000,
        staleTimeout: 10000,
        staleIn: 20000,
      },
    });

    server.route({
      method: 'GET',
      path: '/{platform}/{region}/{tag}/profile',
      options: {
        tags: ['api'],
        validate: {
          params: {
            tag: Joi.string()
              .required()
              // .regex(/^(?:[a-zA-Z\u00C0-\u017F0-9]{3,12}-[0-9]{4,},)?(?:[a-zA-Z\u00C0-\u017F0-9]{3,12}-[0-9]{4,})$/g)
              .description('the battle-tag of the user | "#" should be replaced by an "-"'),
            platform: Joi.string()
              .required()
              .insensitive()
              .valid(['pc', 'xbl', 'psn'])
              .description('the platform that the user use: pc,xbl,psn'),
            region: Joi.string()
              .required()
              .insensitive()
              .valid(['eu', 'us', 'kr', 'cn', 'global'])
              .description('the region the user live is in for example: eu'),
          },
        },
        description: 'Get Stats for a specific hero',
        notes: ' ',
      },
      handler: getProfile,
    });
  },
  name: 'routes-profile',
  pkg: require('../package.json')
};
