'use strict';

const rp = require('request-promise');

const getPatchNotes = function (next) { // eslint-disable-line
  return new Promise(function(resolve, reject) {
    rp('https://cache-us.battle.net/system/cms/oauth/api/patchnote/list?program=pro&region=US&locale=enUS&type=RETAIL&page=1&pageSize=5&orderBy=buildNumber&buildNumberMin=0')
      .then(function(jsonResponse){
        resolve(JSON.parse(jsonResponse));
      })
      .catch(function(){
         const result = {
           statusCode: 404,
           error: 'Found no patch notes'
         };
         resolve(result);
       });
  });
};

exports.plugin = {
  register: (server, options) => { // eslint-disable-line
    server.method('getPatchNotes', getPatchNotes, {
      cache: {
        cache: 'mongo',
        expiresIn: 6 * 10000, // 10 minutes
        generateTimeout: 40000,
        staleTimeout: 10000,
        staleIn: 20000,
      },
    });

    server.route({
      method: 'GET',
      path: '/patch_notes',
      options: {
        tags: ['api'],
        cors: true,
        description: 'Get the latest patch informations',
        notes: ' ',
      },
      handler: getPatchNotes,
    });
  },
  name: 'routes-patch-notes',
  pkg: require('../package.json')
};
