'use strict';

const Glue = require('glue');
const Hapi = require('hapi');

const env = process.env;
let portNumber = 9000;
let hostUrl = 'localhost';
let connectionString = '127.0.0.1:27017';
let collectionName = 'cache';

const server = new Hapi.Server({
  host: hostUrl,
  port: portNumber,
  cache: [
    {
      engine: require('catbox-mongodb'),
      uri: `mongodb://${connectionString}`,
      partition: collectionName,
      name: 'mongo',
    },
  ],
});

// load multiple plugins
server.register([
  require('vision'),
  require('inert'),
  require('./routes/patch_notes.js'),
  require('./routes/achievements.js'),
  require('./routes/allHeroes.js'),
  require('./routes/heroes.js'),
  require('./routes/hero.js'),
  require('./routes/profile.js'),
  require('./routes/get-platforms.js'),
]).then(() => {
  server
    .start()
    .then(() => {}) // if needed
    .catch(err => {
      console.log(err)
    });
});

/*Glue.compose(manifest, options, (err, server) => {
  if (err) {
    throw err;
  }
  server.route({
    method: 'GET', path: '/favicon.ico', handler: { file: 'favicon.ico' }, config: { cache: { expiresIn: 86400000 } },
  });
  if (!module.parent) {
    server.start(() => {
      console.log('\x1b[36m', '╔══════════════════════════════════════════╗', '\x1b[0m');
      console.log('\x1b[36m', '║ LootBox Api - An unoffical Overwatch Api ║', '\x1b[0m');
      console.log('\x1b[36m', '║══════════════════════════════════════════║', '\x1b[0m');
      console.log('\x1b[36m', `║ URL: ${server.info.uri}                  ║`, '\x1b[0m');
      console.log('\x1b[36m', '║                                          ║', '\x1b[0m');
      console.log('\x1b[36m', '╚══════════════════════════════════════════╝', '\x1b[0m');
    });
  } else {
    module.exports = server;
  }
});*/
